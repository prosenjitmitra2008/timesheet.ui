import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-left-panel',
  templateUrl: './left-panel.component.html',
  styleUrls: ['./left-panel.component.scss']
})
export class LeftPanelComponent implements OnInit {

  constructor(
    private router: Router, 
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
  }

  public Logout(){
    this.router.navigate(['./main/login']);
  }

}
