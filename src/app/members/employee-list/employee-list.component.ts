import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import {EmployeeService} from '../../services/employee.service';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss']
})
export class EmployeeListComponent implements OnInit {

  employeeList:any = [];
  weekList: any = [];
  selectedWeek: number;

  constructor(
    private router: Router, 
    private route: ActivatedRoute,
    private empService:EmployeeService
    ) { 
      this.selectedWeek = 1;      
    }

  ngOnInit() {
    this.getWeek();
    this.BindEmployee();
  }

  private getWeek() {
    this.weekList = [
      { Id: 1, WeekName: 'Week 1' },
      { Id: 2, WeekName: 'Week 2' },
      { Id: 3, WeekName: 'Week 3' },
      { Id: 4, WeekName: 'Week 4' }
    ];
  }

  private BindEmployee(){
    this.empService.getallemployees(this.selectedWeek).subscribe(data=>{ 
      this.employeeList = data;
    });
  }

  public weekChange(event){
    this.BindEmployee();
  }

  public view(item: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        Id: item.id
      },
      relativeTo: this.route
    };

    this.router.navigate(['../timesheet'], navigationExtras);
  }

}
