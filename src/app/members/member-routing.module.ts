import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { EmployeeListComponent } from '../members/employee-list/employee-list.component';
import { TimesheetComponent } from '../members/timesheet/timesheet.component';
import { HomeComponent } from './home/home.component';

const adminRoutes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path:'',
        children:[
          { path: '', component: EmployeeListComponent },
          { path: 'employes', component: EmployeeListComponent },
          { path: 'timesheet', component: TimesheetComponent }
        ]
      }      
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class MemberRoutingModule { }
