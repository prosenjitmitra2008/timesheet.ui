import { MemberRoutingModule } from './member-routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { EmployeeListComponent } from '../members/employee-list/employee-list.component';
import { TimesheetComponent } from '../members/timesheet/timesheet.component';
import { HomeComponent } from './home/home.component';

import { HeaderComponent } from '../common/header/header.component';
import { FooterComponent } from '../common/footer/footer.component';
import { LeftPanelComponent } from '../common/left-panel/left-panel.component';

@NgModule({
  imports: [
    CommonModule,
    MemberRoutingModule,
    FormsModule
  ],
  declarations: [
    EmployeeListComponent,
    TimesheetComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    LeftPanelComponent
  ]
})
export class MemberModule { }
