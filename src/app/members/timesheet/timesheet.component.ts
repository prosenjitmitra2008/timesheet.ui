import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { EmployeeService } from '../../services/employee.service';
import { TaskService } from '../../services/task.service';
import { TaskDetailsService } from '../../services/taskDetails.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.scss']
})
export class TimesheetComponent implements OnInit {

  employeeList: any = [];
  taskList: any = [];
  weekList: any = [];
  timesheetDetails: any = [];
  mod: any = { Name: '', Description: '' };
  selectedValue: number;
  selectedWeek: number;
  Id: string;
  closeResult: string;
  sundayTotal: number = 0;
  mondayTotal: number = 0;
  tuesdayTotal: number = 0;
  wednesdayTotal: number = 0;
  thursdayTotal: number = 0;
  fridayTotal: number = 0;
  saturdayTotal: number = 0;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private empService: EmployeeService,
    private taskService: TaskService,
    private taskDetailsService: TaskDetailsService,
    private modalService: NgbModal
  ) {
    this.selectedWeek = 1;
    this.route.queryParams.subscribe(params => {
      this.Id = params["Id"];
    });
  }

  ngOnInit() {
    this.selectedValue = parseInt(this.Id);

    this.getEmployee();
    this.getTask();
    this.getWeek();
    this.getTimesheetDetails();
  }

  ngAfterViewInit() {
  }

  private getEmployee() {
    this.empService.getallemployees(this.selectedWeek).subscribe(data => {
      this.employeeList = data;
    });
  }

  private getTask() {
    this.taskService.getallTask().subscribe(data => {
      this.taskList = data;
    });
  }

  private getWeek() {
    this.weekList = [
      { Id: 1, WeekName: 'Week 1' },
      { Id: 2, WeekName: 'Week 2' },
      { Id: 3, WeekName: 'Week 3' },
      { Id: 4, WeekName: 'Week 4' }
    ];
  }

  private getTimesheetDetails() {

    this.taskDetailsService.getTasks(this.selectedValue, this.selectedWeek).subscribe(data => {
      this.timesheetDetails = data;
      console.log(this.timesheetDetails);

      //Clear variable..
      this.sundayTotal = 0;
      this.mondayTotal = 0;
      this.tuesdayTotal = 0;
      this.wednesdayTotal = 0;
      this.thursdayTotal = 0;
      this.fridayTotal = 0;
      this.saturdayTotal = 0;

      this.timesheetDetails.forEach(element => {
        this.sundayTotal = this.sundayTotal + element.sunday;
        this.mondayTotal = this.mondayTotal + element.monday;
        this.tuesdayTotal = this.tuesdayTotal + element.tuesday;
        this.wednesdayTotal = this.wednesdayTotal + element.wednesday;
        this.thursdayTotal = this.thursdayTotal + element.thursday;
        this.fridayTotal = this.fridayTotal + element.friday;
        this.saturdayTotal = this.saturdayTotal + element.saturday;
        element.userId = this.Id;
        element.weekId = this.selectedWeek;
      });
    });
  }

  public selectChange(event) {
    this.getTimesheetDetails();
  }

  public weekChange(event) {
    this.getTimesheetDetails();
  }

  public save() {
    this.taskDetailsService.addTask(this.timesheetDetails).
      subscribe(data => {
        Swal.fire('Success', 'Task Update Successfully!!!', 'success');
        this.getTimesheetDetails();
      });
  }

  public back() {
    this.router.navigate(['../employes'], { relativeTo: this.route });
  }

  public OpenTaskPopup(content: any) {
    this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.getTask();
      this.getTimesheetDetails();
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  public saveTask() {
    console.log(this.mod);
    if (this.mod.Name == "") {
      Swal.fire('Error', 'Please enter task name!!!', 'error');
    }
    else if (this.mod.Description == "") {
      Swal.fire('Error', 'Please enter task description!!!', 'error');
    }
    else {
      this.taskService.addTask(this.mod).
        subscribe(data => {
          Swal.fire('Success', 'Task Insert Successfully!!!', 'success');
          this.modalService.dismissAll();
        });
    }
  }

}
