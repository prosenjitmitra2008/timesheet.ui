import { NgModule } from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { LoginComponent } from '../public/login/login.component';
import { RegisterComponent } from '../public/register/register.component';
import { PublicRoutingModule } from './public-routing.module';
import { HomeComponent } from './home/home.component';

@NgModule({
  imports: [
    CommonModule,
    PublicRoutingModule,
    FormsModule
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    HomeComponent
  ]
})
export class PublicModule { }
