import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class EmployeeService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getallemployees(selectedWeek: number) {
        return this.http.get(this.baseapi + "/employee/getAll?selectedWeek=" + selectedWeek);
    }
}