import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class TaskService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getallTask() {
        return this.http.get(this.baseapi + "/Task/getAll");
    }

    addTask(task: any) {
        let bodyString = JSON.stringify(task);
        let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json;charset=utf-8', });
        let options = { headers: httpHeaders }
        return this.http.post(this.baseapi + "/Task/addTask", task, options);
    }
}