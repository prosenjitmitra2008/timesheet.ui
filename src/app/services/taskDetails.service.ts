import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class TaskDetailsService {
    private baseapi = environment.apiUrl;
    constructor(private http: HttpClient) { }

    getTasks(employeeId: number, weekId: number) {
        return this.http.get(this.baseapi + "/TaskSheet/getAll?employeeId=" + employeeId + "&weekId=" + weekId)
    }  

    addTask(taskDetails: any) {
        let bodyString = JSON.stringify(taskDetails);
        let httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json;charset=utf-8', });
        let options = { headers: httpHeaders }
        return this.http.post(this.baseapi + "/TaskSheet/addTask", taskDetails, options);
    }
}